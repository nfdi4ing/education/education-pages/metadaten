## 7. Übung - Roboterarm

* **Experiment**: Roboterarm (Modell CXY-3250 Firma X. AG) wurde eingekauft. Mittels Software Y (Version 12.4.4) wurde eine Routine aufgespielt. Diese erlaubt mit dem angebrachten Werkzeug (Flaschenöffner Model Open von Firma Trink AG) Flaschen zu öffnen. Erstellt wurden Videoaufnahmen mit Kamera (Click von Firma S AG)

* Die Forschungsdaten (hier: Videoaufnahmen) werden zusammen mit den Metadaten an ein Repositorium übergeben (z.B. Universitätsbibliothek). Dort werden DOIs vergeben und (Meta)daten auffindbar und zugänglich gemacht (siehe FAIR)

* Verwendet wird die SimpleRobot-Ontologie (fiktiv!) für die Forschungs(meta-)Daten und das DataCite-Schema für den Forschenden
    * Aufgabe 1: Metadaten in die Ontologien eintragen.
    * Aufgabe 2: Welche Metadatenfelder fehlen? Wie kann die Ontologie erweitert werden?

<br>
<br>
<br>
<br>

*nach: Benjamin Farnbacher, Nils Hoppe, Katja Kessler, Stephan Peinkofer, Stephan Hachinger, & Christian Stemmer. (2021, July 22). Forschungsdatenmanagement für Daten aus High Performance Measurement and Computing an der TU München - Best-Practice Beispiele und Anwendungen. Zenodo. https://doi.org/10.5281/zenodo.5139408*


#### Beispiel: DataCite Metadata Schema for Research Output

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/metadaten/-/raw/master/media_files/beispiel1.png" alt="" width="" height=""> 


#### Beispiel: DataCite Metadata Schema for Research Output (Lösung)

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/metadaten/-/raw/master/media_files/beispiel1_loesung.png" alt="" width="" height=""> 


#### Beispiel: SimpleRobot-Fantasie-Ontologie

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/metadaten/-/raw/master/media_files/beispiel2.png" alt="" width="" height="">


#### Beispiel: SimpleRobot-Fantasie-Ontologie (Lösung)

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/metadaten/-/raw/master/media_files/beispiel2_loesung.png" alt="" width="" height=""> 

*nach: Benjamin Farnbacher, Nils Hoppe, Katja Kessler, Stephan Peinkofer, Stephan Hachinger, & Christian Stemmer. (2021, July 22). Forschungsdatenmanagement für Daten aus High Performance Measurement and Computing an der TU München - Best-Practice Beispiele und Anwendungen. Zenodo. https://doi.org/10.5281/zenodo.5139408*