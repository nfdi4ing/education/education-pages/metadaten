## Inhaltsverzeichnis

1. [Ziele des Trainings](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#/2)
2. [Definition Metadaten](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#/3)
3. [Metadaten im Datenlebenszyklus](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#/4)
4. [Metadaten als Schlüssel zu FAIR](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#5)
5. [Standards](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#/6)
6. [Best-Practice](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#/7)
7. [Übung](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadaten/html_slides/metadaten.html#/8)

