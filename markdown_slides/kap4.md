## 4. Metadaten sind der Schlüssel zu FAIRen Daten

#### Metadaten machen Daten FAIR

Der hier unten aufgeführte Artikel, der die FAIR-Prinzipien erstmals wissenschaftlich dokumentierte, betont die zunehmende Anzahl und Bedeutung der Stakeholder in Bezug auf Datenerhebung, -verarbeitung und -speicherung, was die Notwendigkeit klarer Richtlinien und Leitfäden hervorrief. Es ist wichtig anzumerken, dass die FAIR-Prinzipien darauf abzielen, die Fähigkeiten von Maschinen zur besseren Auffindbarkeit und Nutzung von Daten zu verbessern, was nicht nur für die Daten selbst, sondern auch für die zugrundeliegenden Algorithmen, Tools und Workflows gilt.

Die FAIR-Prinzipen sind:

* Findable
* Accessible
* Interoperable
* Reusable

<br>
<br>
<br>
<br>
<br>
<br>
<br>

*Weiterführende Informationen:*
*FAIR Guiding Principles*
*Wilkinson, M., Dumontier, M., Aalbersberg, I. et al. The FAIR Guiding Principles for scientific data management and stewardship. Sci Data 33, 160018 (2016). https://doi.org/10.1038/sdata.2016.18* 


### Auffindbarkeit (to be Findable)

* F1. (**Meta**)**Daten** wird ein global eindeutiger und dauerhaft persistenter Identifier zugewiesen
* F2. Daten werden mit umfangreichen **Metadaten** (vergl. R.1) beschrieben
* F3. **Metadaten** werden in einem durchduchbaren Verzeichnis registriert oder indiziert
* F4. **Metadaten** erhalten klar und eindeutig den Identifier, der die Daten referenziert

<br>

**Machen Sie Ihre Daten über Metadaten auffindbar. Der persistente Identifier ist ein essentielles Metadatum zur Auffindbarkeit Ihrer Daten!**

<br>
<br>
<br>
<br>
<br>
<br>
<br>

*Quelle: Kraft, Angelina (12.11.2017): Die FAIR Data Prinzipien für Forschungsdaten. TIB Blog, https://blogs.tib.eu/wp/tib/2017/09/12/die-fair-data-prinzipien-fuer-forschungsdaten/ (Zugriff am 04.04.2022)*


### Zugänglichkeit (to be Accessible)

* A1. **(Meta)Daten** sind über Ihren Identifier mithilfe eines standardisierten Kommunikationsprotokolls auffindbar
    * A1.1 Das Protokoll ist offen, frei und universell implementierbar
    * A1.2 Das Protokoll unterstützt, wo notwendig, die Authentifizierung und Rechteverwaltung
* A2. **Metadaten** sind/bleiben verfügbar, auch für den Fall, dass die zugehörigen Forschungsdaten nicht mehr vorhanden sind

<br>

**Machen Sie Ihre Daten über ein Repositorium zugänglich!**

<br>
<br>
<br>
<br>
<br>
<br>
<br>

*Quelle: Kraft, Angelina (12.11.2017): Die FAIR Data Prinzipien für Forschungsdaten.TIB Blog, https://blogs.tib.eu/wp/tib/2017/09/12/die-fair-data-prinzipien-fuer-forschungsdaten/ (Zugriff am 04.04.2022)*


### Interoperabilität (to be Interoperable)

* I1. **(Meta)daten** nutzen eine formale, zugängliche, gemeinsam genutzte und breit anwendbare Sprache für die Wissensrepräsentation
* I2. **(Meta)daten** benutzen Vokabulare, welche den FAIR Prinzipien folgen
* I3. **(Meta)daten** enthalten qualifizierte Referenzen auf andere (Meta)Daten

<br>

**Standardisierte Vokabulare sind der Schlüssel zur Interoperabilität!**

<br>
<br>
<br>
<br>
<br>
<br>
<br>

*Quelle: Kraft, Angelina (12.11.2017): Die FAIR Data Prinzipien für Forschungsdaten.TIB Blog, https://blogs.tib.eu/wp/tib/2017/09/12/die-fair-data-prinzipien-fuer-forschungsdaten/ (Zugriff am 04.04.2022)*


### Wiedervewendbarkeit (to be Reusable)

* R1. **(Meta)Daten** sind detailliert beschrieben und enthalten präzise, relevante Attribute
    * R1.1 **(Meta)Daten** enthalten eine eindeutige, zugreifbare Angabe einer Nutzungslizenz
    * R1.2 **(Meta)Daten** enthalten detaillierte Provenienz-Informationen
    * R1.3 **(Meta)Daten** entsprechen den fachgebietsrelevanten Community Standards

<br>

**Beschreiben Sie Ihre Daten mit nachnutzungsrelevanten Metadaten, die Lizenz ist ein wichtiges Metadatum, NFDI4Ing arbeitet an einem Standard für ingenieurwissenschaftliche Metadaten!**

<br>
<br>
<br>
<br>
<br>
<br>
<br>

*Quelle: Kraft, Angelina (12.11.2017): Die FAIR Data Prinzipien für Forschungsdaten.TIB Blog, https://blogs.tib.eu/wp/tib/2017/09/12/die-fair-data-prinzipien-fuer-forschungsdaten/ (Zugriff am 04.04.2022)*