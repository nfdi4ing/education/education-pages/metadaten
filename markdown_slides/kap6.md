## 6. Best-Practice

### Metadatenschemata und Metadatenstandards

**Was ist ein Metadatenschema?**

Ein Metadatenschema beschreibt die Struktur von Metadaten und bestimmt, welche Elemente zur Beschreibung der Forschungsdaten verpflichtend sind und in welchem Datenformat diese angegeben werden sollen. Es gibt auch disziplinspezifische Schemata, die spezielle Elemente behandeln, die für eine bestimmte Disziplin spezifisch sind oder von ihr benötigt werden.

**Was ist ein Metadatenstandard?**

Ein Metadatenstandard legt Regeln und Richtlinien für die Erstellung und Verwendung von Metadaten fest, also z.B welche und wie viele Zeichen zulässig sein sollten. Metadatenstandards sorgen für Vergleichbarkeit, erhöhen die Interoperabilität und ermöglichen Maschinenlesbarkeit.
<br>
Metadatenstandards bieten also einen Rahmen für die Erstellung von Metadaten, während Metadatenschemata konkrete Vorgaben für die Implementierung dieser Standards liefern.

**In welchen Dateiformaten werden Metadaten gespeichert?**

Die gängigsten Dateiformate für Metadaten sind XML, JSON und RDF. <br>
    * **XML** steht für **eXtensible Markup Language** und wurde für die Speicherung und den Transport von Daten so entwickelt, dass sie sowohl von Menschen als auch Maschinen gelesen werden kann. <br>
    * **JSON** steht für **J**ava**S**cript **O**bject **N**otation und eignet sich zum Speichern und Übertragen von Daten. Es wird häufig verwendet, wenn Daten von einem Server auf an eine andere Webseite gesendet werden und ist leicht zu verstehen. <br>
    * **RDF** ist ein Standardmodell für den Datenaustausch im Web. RDF verfügt über Funktionen, die die Zusammenführung von Daten erleichtern, selbst wenn sich die zugrunde liegenden Schemata unterscheiden

https://forschungsdaten.info/praxis-kompakt/glossar/; 
https://zenodo.org/records/2660187;
https://www.teamnext.de/blog/xmp-vs-iptc/; 
https://www.w3schools.com/xml/#gsc.tab=0; 
https://www.w3schools.com/whatis/whatis_json.asp; 
https://www.w3.org/RDF/


**Welche Metadatenschmata gibt es?**

Zu den verbreiteten disziplinübergreifende Metadatenschemata sind gehören:

**1. Dublin Core**
* [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dces/), früher bekannt als Dublin Core Metadata Element Set, ist ein internationales Metadatenschema, das von der Dublin Core Metadata Initiative (DMCI), einer unabhängigen, öffentlichen und gemeinnützigen Organisation, gepflegt wird. Das Dublin-Core-Schema wird im Allgemeinen zur Beschreibung digitaler und physischer Ressourcen verwendet. Der Dublin Core ist ein allgemeiner Standard, der zunächst von Bibliotheken verwendet wurde und für bestimmte Disziplinen angepasst werden kann.

* DCMI besteht aus 15 Elementen (bekannt als Dublin Core Metadata Element Set) sowie aus "Erweiterungsvokabularen", die Dutzende von Eigenschaften, Klassen, Datentypen und Kodierungsschemata für das Vokabular umfassen.

* Die 15 Metadatenelemente in Simple Dublin Core sind:
Titel, Autor, Thema, Beschreibung, Herausgeber, Mitwirkende, Datum, Typ, Format, Identifier, Quelle, Sprache, Beziehung, Geltungsbereich, Rechte


**2. MODS (Metadata Object Description Schema)**
* Das Metadata Object Description Schema (MODS) ist ein XML-Schema mit MARC*-ähnlicher Semantik. MODS wurde von der Library of Congress aus der Notwendigkeit heraus entwickelt, eine Möglichkeit für die Beschreibung komplexer digitaler Objekte zu finden, die leichter zu erlernen ist als MARC und umfangreicher ist als der Dublin Core.
MODS ist anwendungsfreundlicher, weil es sprachbasierte Tags und nicht die bei MARC üblichen numerischen Codes verwendet. Es gibt 20 MODS-Elemente auf oberster Ebene, von denen viele Unterelemente für eine detailliertere Beschreibung enthalten.

* [MODS User Guidelines](https://www.loc.gov/standards/mods/userguide/)

* [Ein Beispiel  zur Anwendung des MODS-Schemas und weitere Metadatenschemata.](https://guides.library.cmu.edu/c.php?g=472661&p=9230176)

<br>

*MARC-Format (maschinenlesbare Katalogisierung) ist ein Standardsatz digitaler Formate für die maschinenlesbare Beschreibung von Objekten, die von Bibliotheken katalogisiert werden, wie z.B. Bücher und DVDs.
Guidelines


**Wie finde ich ein passendes Metadatenschema?**

Um geeignete Metdatenschemata zu finden, kann das [Metadatenverzeichnis der Research Data Alliance (RDA)](https://www.dcc.ac.uk/guidance/standards/metadata) durchsucht werden. 

**Wie erstelle ich mein eigenes Metadatenschema?**

Es besteht auch die Möglichkeit, ein individuelles Metadatenschema zu entwickeln. Es ist jedoch ratsam, vorher gründlich zu prüfen, ob dies wirklich notwendig ist oder ob ein bereits bestehendes Schema den Anforderungen des eigenen Projekts genügt. Zu beachten ist, dass ein eigenes Metadatenschema kontinuierlich über die gesamte Lebensdauer der Daten hinweg verwaltet werden muss.

Die groben Schritte für die Erstellung eines individuellen Metadatenschemas können so aussehen:

1. Den Geltungsbereich des Schemas bestimmen
    * Welche Art von Daten soll beschrieben werden, in welchem System wird das Schema verwendet, für wen soll das Schema gelten, etc.
2. Informationen sammeln
    * Informieren über Zwecke, Prinzipien und Aufbau und Pflege von Schemata
    * Ähnliche Projekte aus dem eigenen oder aus ähnlichen Fachgebieten analysieren
3. Struktur des Schemas bestimmen
    * Entscheidung über einen Satz von Metadatenelementen (einzelne Entität) oder eine Gruppe von Metadatenelementen (mehrere Entitäten)
4. Registrierung des Schemas bei den zuständigen Stellen
5. Identifizierung nützlicher Elementgruppen und Kodierungsschemata
    * Welche bestehenden Elemente, Gruppierungen von Elementen und Unterelementen aus anderen Schemata können in dem Schema verwendet werden?

<br>

Detailliertere Informationen zur Erstellung eines eigenen Metadatenschemas sind in diesem Dokument der ISO zu finden: <br>
https://committee.iso.org/files/live/sites/tc46sc11/files/documents/N800R1%20Where%20to%20start-advice%20on%20creating%20a%20metadata%20schema.pdf


### Shapes Constraint Language (SHACL)

SHACL ist eine standardisierte Sprache des World Wide Web Consortiums (W3C) zur Beschreibung und Einschränkung von RDF-Graphen. RDF-Graphen sind Datenmodelle, die verwendet werden, um Informationen im Web darzustellen. SHACL kann verwendet werden, um sicherzustellen, dass RDF-Daten bestimmten Regeln und Einschränkungen entsprechen.
<br>

[Weiterführende Informationen über SHACL vom W3C finden Sie hier.](https://www.w3.org/TR/shacl/)
<br>

Der [NFDI4Ing Metadata Profile Service](https://nfdi4ing.de/1-24-2/) ist eine Plattform, die die Erstellung, Pflege und gemeinsame Nutzung von Metadatenprofilen erleichtert. Die Profile basieren auf der W3C-Empfehlung SHACL und können über eine grafische Benutzeroberfläche durch Auswahl geeigneter Begriffe aus bestehenden Ontologien erstellt werden.