## 2. Definition Metadaten

#### Daten über Daten

*"Metadaten bezeichnen alle zusätzlichen Informationen, die zur Interpretation der eigentlichen Daten, z.B. Forschungsdaten notwendig oder sinnvoll sind und die eine (automatische) Verarbeitung der Forschungsdaten durch technische Systeme ermöglichen."*

~ https://www.forschungsdaten.org/index.php/Metadaten

<br>

**Ideale Metadaten sind:**

* strukturiert
* maschinenlesbar
* standardisiert


#### Beispiel: Metadatentabelle / Relational Database

Um "Daten über Daten" etwas zu veranschaulichen, ist hier ein fiktives Beispiel einer Datenerhebung zu sehen.

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/metadaten/-/raw/master/media_files/bild_metadatentabelle.png" alt="" width="" height=""> 

In diesem Kontext präsentiert sich eine Tabelle, welche Informationen über eine eindeutige Identifikationsnummer (ID), das Datum der Erfassung, das Gewicht der Einheit sowie den zugehörigen Hersteller enthält. Ohne die ergänzende Tabelle mit Metadaten könnte die Interpretation der Daten erschwert sein. Beispielsweise bleibt unklar, welche spezifische Bedeutung dem Datum zukommt – es könnte sich um das Liefer-, Verkaufs- oder Produktionsdatum handeln. Zudem bleibt die Einheit des Gewichts und die Identität der Zahlen in der Hersteller-Spalte unklar. Durch das Hinzufügen der Metadaten-Tabelle wird jedoch die Interpretation der Daten erheblich erleichtert. Nun wird klar, dass das Datum den Produktionszeitpunkt repräsentiert, das Gewicht in Gramm angegeben ist und die Zahlen in der Hersteller-Spalte spezifischen Herstellern zugeordnet werden können.
