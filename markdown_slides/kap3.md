## 3. Metadaten im Datenlebenszyklus

<iframe width="560" height="315" src=https://www.youtube.com/embed/1eCMRhbi60U frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope" allowfullscreen>
</iframe>

https://www.youtube.com/watch?v=1eCMRhbi60U <br>
Dominik Schmitz, Daniela Hausen, Ute Trautwein-Bruns. Forschungsdaten und ihre Metadaten. <br>
RWTH Aachen University. 2018. Verfügbar unter DOI: 10.18154/RWTH-2018-231101 


### Daten brauchen Metadaten

**Metadaten helfen...**

* Forschungsdaten zu verstehen
* Forschungsdaten zu nutzen
* Forschungsdaten zu finden
* Forschungsdaten zu verwalten