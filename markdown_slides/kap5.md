## 5. Standards

### Kontrollierte Vokabulare

**Standardisierte Metadaten und Vokabulare als Schlüssel zur Interoperabilität**

Kontrollierte Vokabulare sind systematische Sammlungen von Begriffen, die nach festgelegten Regeln und Richtlinien bearbeitet wurden, um die Mehrdeutigkeiten der natürlichen Sprache zu reduzieren. 
Die verschiedenen Typen kontrollierter Vokabulare sind in der Grafik dargestellt.  

<img src="https://git.rwth-aachen.de/nfdi4ing/education/education-pages/metadaten/-/raw/master/media_files/metadaten_standards.png" alt="" width="" height="">


* **Index- oder Stichwortlisten** sind automatisch generierte Sammlungen von Wörtern, die beispielsweise aus dem Titel oder Abstract eines Dokuments extrahiert werden. Diese Listen unterliegen keiner terminologischen Kontrolle und zählen daher nicht zu den kontrollierten Vokabularen.

* Ein **Thesaurus** ist ein kontrolliertes und strukturiertes Vokabular zur Indexierung und zum Retrieval, das Relationen zwischen Synonymen, Vorzugsbenennungen, hierarchischen und thematisch verwandten Begriffen abbildet. Ähnlich wie ein Wörterbuch besteht ein Thesaurus aus Begriffen der natürlichen Sprache, oft aus einer Fachsprache, die nach festen Regeln terminologisch kontrolliert werden. Die Terme im Thesaurus repräsentieren Konzepte, zwischen denen hierarchische und assoziative Relationen aufgebaut werden, wodurch die Struktur des Thesaurus entsteht.

* Eine **Taxonomie** organisiert eine Gruppe von Gegenständen durch Kategorisierung oder Klassifizierung in eine hierarchische Struktur.

* Eine **Ontologie** ist in der Informatik eine Methode zur Wissensdarstellung, die darauf abzielt, Wissen so zu formulieren, dass es von Maschinen interpretiert und verarbeitet werden kann. Dies geschieht durch die Definition von Entitäten und deren Beziehungen in Form von expliziten Aussagen, was als semantische Tripel bezeichnet wird. Ein semantisches Tripel besteht aus drei Komponenten: Subjekt, Prädikat und Objekt, die zusammen eine Beziehung zwischen Entitäten beschreiben.

https://www2.bui.haw-hamburg.de/pers/ulrike.spree/remind/vokabulare.htm
https://www.tecislava.com/blog/ontology 


### Fachspezifische Metadaten für bessere Nachnutzbarkeit

**Ingenieurwissenschaftliche Standards und Metadaten**

* NFDI4Ing erarbeitet eine Ontologie für Ingenieurwissenschaften

    NFDI4Ing S3 [metadata and terminology services](https://nfdi4ing.de/base-services/s-3/) <br>
    GitLab Projekt [metdata4ing](https://git.rwth-aachen.de/nfdi4ing/metadata4ing/metadata4ing)

<br>

**Finden von Standards für die Ingenieurwissenschaften**

* https://fairsharing.org/search?q=engineering